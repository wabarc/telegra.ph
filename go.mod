module github.com/wabarc/telegra.ph

go 1.15

require (
	github.com/google/uuid v1.2.0 // indirect
	github.com/kallydev/telegraph-go v1.0.0
	github.com/oliamb/cutter v0.2.2
	github.com/wabarc/helper v0.0.0-20210407153720-1bfe98b427fe
	github.com/wabarc/imgbb v1.0.0
	github.com/wabarc/logger v0.0.0-20210417045349-d0d82e8e99ee
	github.com/wabarc/screenshot v1.1.0
	golang.org/x/sys v0.0.0-20210419170143-37df388d1f33 // indirect
)
